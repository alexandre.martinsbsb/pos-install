#!/usr/bin/env bash

echo -e "Iniciando script"

## CORES
VERMELHO='\e[1;91m'
VERDE='\e[1;92m'
SEM_COR='\e[0m'

echo -e "${VERDE}[INFO] - Atualizando o sistema${SEM_COR}"
sudo rm -rf /var/lib/apt/lists/* && sudo rm -rf /var/lib/apt/lists/partial/* && sudo rm -f /var/lib/dpkg/lock && sudo apt update -y && sudo apt list --upgradable && sudo apt upgrade -y && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo apt autoclean -y && sudo apt clean -y sudo flatpak repair && sudo flatpak update -y && sudo flatpak uninstall --unused -y

echo -e "${VERDE}[INFO] - Removendo aplicativos${SEM_COR}"

sudo apt remove gnome-calendar cheese five-or-more gnome-contacts simple-scan evolution gnome-shell gnome-nibbles gnome-sound-recorder hitori gnome-klotski lightsoff gnome-mahjongg gnome-maps gnome-mines aisleriot quadrapassel four-in-a-row iagno rhythmbox gnome-sudoku swell-foop tali gnome-taquin gnome-tetravex gnome-chess -y && sudo apt purge gnome-calendar cheese five-or-more gnome-contacts simple-scan evolution gnome-shell gnome-nibbles gnome-sound-recorder hitori gnome-klotski lightsoff gnome-mahjongg gnome-maps gnome-mines aisleriot quadrapassel four-in-a-row iagno rhythmbox gnome-sudoku swell-foop tali gnome-taquin gnome-tetravex gnome-chess -y && sudo apt autoremove -y && sudo apt clean -y

#finalizacao
echo -e "${VERDE}[INFO] - Script finalizado, instalacao concluida! :)${SEM_COR}"
